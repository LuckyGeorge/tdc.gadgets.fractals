using System;

namespace TDC.Gadgets.Fractals.Tools
{
    public static class VectorTools
    {
        public static double[] Add(this double[] a, double[] b)
        {
            var retVal = new[] { a[0] + b[0], a[1] + b[1], a[2] + b[2] };
            return retVal;
        }

        public static double[] Substract(this double[] a, double[] b)
        {
            var retVal = new[] { a[0] - b[0], a[1] - b[1], a[2] - b[2] };
            return retVal;
        }

        public static double[] Multiply(this double[] a, double b)
        {
            var retVal = new[] { a[0] * b, a[1] * b, a[2] * b };
            return retVal;
        }

        public static double[] Multiply(this double[] a, double[] b)
        {
            var retVal = new[] { a.Dot(new[] { b[0], 0, 0 }), a.Dot(new[] { 0, b[1], 0 }), a.Dot(new[] { 0, 0, b[2] }) };
            return retVal;
        }

        public static double[] Divide(this double[] a, double b)
        {
            var divB = 1D / b;
            var retVal = a.Multiply(divB);
            return retVal;
        }

        public static double Dot(this double[] a, double[] b)
        {
            var retVal = a[0] * b[0] +  a[1] * b[1] + a[2] * b[2];
            return retVal;
        }

        public static double Length(this double[] a)
        {
            var retVal = Math.Sqrt(a.Dot(a));
            return retVal;
        }

        public static double Length2(this double[] a)
        {
            var retVal = a.Dot(a);
            return retVal;
        }

        public static double[] Cos(this double[] a)
        {
            var retVal = new[] { Math.Cos(a[0]), Math.Cos(a[1]), Math.Cos(a[2]) };
            return retVal;
        }
    }
}