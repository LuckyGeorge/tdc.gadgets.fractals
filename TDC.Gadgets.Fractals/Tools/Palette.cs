﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace TDC.Gadgets.Fractals.Tools
{
    public enum PaletteType
    {
        Rainbow1,
        Rainbow2,
        Rainbow3,
        Rainbow4,
        BlueCyan,
        BlueMagentaOrange,
        BlueWhiteRed,
        CyanMagenta,
        GreenBlueOrange,
        GreenCyan,
        GreenMagenta,
        GreenRed,
        MagentaGreen,
        OrangeBlue,
        OrangeMagentaBlue,
        RedBlue,
        YellowGreenBlue,
        YellowMagentaCyan,
        YellowPurpleMagenta,
        YellowRed,
        DesertNightSky,
        GreenSeaSunset,
        WarmNorthernLight,
        WarmAutumn,
        NorthernLight,
        AutumnLeaf,

    }
    public static class Palette
    {
        /// <summary>
        /// Parameter von http://dev.thi.ng/gradients/
        /// </summary>
        private static readonly double[][][] Parameters = {
            new[]
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {1.0, 1.0, 1.0},new [] { 0.00, 0.333, 0.666 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.5},new [] {0.666, 0.666, 0.666},new [] {1.0, 1.0, 1.0},new [] { 0.00, 0.333, 0.666 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.5},new [] {0.75, 0.75, 0.75},new [] {1.0, 1.0, 1.0},new [] { 0.00, 0.333, 0.666 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.5},new [] {1.0, 1.0, 1.0},new [] {1.0, 1.0, 1.0},new [] { 0.00, 0.333, 0.666 }
            },
            new[]
            {
                new []{0.0, 0.5, 0.5},new [] {0.0, 0.5, 0.5},new [] {0.0, 0.5, 0.333},new [] { 0.0, 0.5, 0.666 }
            },
            new[]
            {
                new []{0.938, 0.328, 0.718},new [] {0.659, 0.438, 0.328},new [] {0.388, 0.388, 0.296},new [] { 2.538, 2.478, 0.168 }
            },
            new[]
            {
                new []{0.66, 0.56, 0.68},new [] {0.718, 0.438, 0.72},new [] {0.52, 0.80, 0.52},new [] { -0.43, -0.397, -0.083 }
            },
            new[]
            {
                new []{0.61, 0.498, 0.65},new [] {0.388, 0.498, 0.35},new [] {0.53, 0.498, 0.62},new [] { 3.438, 3.012, 4.025 }
            },
            new[]
            {
                new []{0.892, 0.725, 0.0},new [] {0.878, 0.278, 0.725},new [] {0.332, 0.518, 0.545},new [] { 2.44, 5.043, 0.732 }
            },
            new[]
            {
                new []{0.0, 0.5, 0.5},new [] {0.0, 0.5, 0.5},new [] {0.0, 0.333, 0.5},new [] { 0.0, 0.666, 0.5 }
            },
            new[]
            {
                new []{0.666, 0.5, 0.5},new [] {0.5, 0.666, 0.5},new [] {0.666, 0.666, 0.5},new [] { 0.2, 0.0, 0.5 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.0},new [] {0.5, 0.5, 0.0},new [] {0.5, 0.5, 0.0},new [] { 0.5, 0.0, 0.0 }
            },
            new[]
            {
                new []{0.59, 0.811, 0.12},new [] {0.41, 0.392, 0.59},new [] {0.94, 0.548, 0.278},new [] { -4.242, -6.611, -4.045 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {0.8, 0.8, 0.5},new [] { 0.0, 0.2, 0.5 }
            },
            new[]
            {
                new []{0.821, 0.328, 0.242},new [] {0.659, 0.481, 0.896},new [] {0.612, 0.34, 0.296},new [] { 2.82, 3.026, -0.273 }
            },
            new[]
            {
                new []{0.5, 0.0, 0.5},new [] {0.5, 0.0, 0.5},new [] {0.5, 0.0, 0.5},new [] { 0.0, 0.0, 0.5 }
            },
            new[]
            {
                new []{0.65, 0.5, 0.31},new [] {-0.65, 0.5, 0.6},new [] {0.333, 0.278, 0.278},new [] { 0.666, 0.0, 0.666 }
            },
            new[]
            {
                new []{0.1, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {0.75, 1.0, 0.666},new [] { 0.8, 1.0, 0.333 }
            },
            new[]
            {
                new []{0.731, 1.01, 0.192},new [] {0.358, 1.01, 0.666},new [] {1.08, 0.36, 0.328},new [] { 0.965, 2.265, 0.837 }
            },
            new[]
            {
                new []{0.5, 0.5, 0.0},new [] {0.5, 0.5, 0.0},new [] {0.1, 0.5, 0.0},new [] { 0.0, 2.0, 0.0 }
            },
            new []
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {1.0, 1.0, 1.0},new [] { 0.00, 0.10, 0.20 }
            },
            new []
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {1.0, 1.0, 1.0},new [] { 0.30, 0.20, 0.20 }
            },
            new []
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {1.0, 1.0, 0.5},new [] { 0.80, 0.90, 0.30 }
            },
            new []
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {1.0, 0.7, 0.4},new [] {0.0, 0.15, 0.2}
            },
            new []
            {
                new []{0.5, 0.5, 0.5},new [] {0.5, 0.5, 0.5},new [] {2.0, 1.0, 0.0},new [] { 0.50, 0.20, 0.25 }
            },
            new []
            {
                new []{0.8, 0.5, 0.4},new [] {0.2, 0.4, 0.2},new [] {2.0, 1.0, 1.0},new [] { 0.00, 0.25, 0.25 }
            },
        };

        public static int MaxParameters => Parameters.GetLength(0);

        public static byte[] GetColor(double iteration, double maxIterations, PaletteType palette = PaletteType.WarmAutumn)
        {
            var parameterSet = (int) palette;

            var value = iteration / maxIterations;
            var color = GenerateColor(value,
                Parameters[parameterSet][0],
                Parameters[parameterSet][1],
                Parameters[parameterSet][2],
                Parameters[parameterSet][3]);
            return new[] {(byte) (color[0] * 255), (byte) (color[1] * 255), (byte) (color[2] * 255)};
        }

        /// <summary>
        /// Formel von http://www.iquilezles.org/www/articles/palettes/palettes.htm
        /// </summary>
        /// <param name="value"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        private static double[] GenerateColor(double value, double[] a, double[] b, double[] c, double[] d)
        {
            //return a + b*cos( 6.28318*(c*t+d) );
            var ct = c.Multiply(value);
            var ctd = ct.Add(d);
            var ctdpi = ctd.Multiply(6.28318);
            var cosctdpi = ctdpi.Cos();
            var bcosctdpi = b.Multiply(cosctdpi);
            var abcosctdpi = a.Add(bcosctdpi);
            return abcosctdpi;
        }
    }
}