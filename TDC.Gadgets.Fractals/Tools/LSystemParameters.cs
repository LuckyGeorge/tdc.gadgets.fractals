﻿using System.Collections.Generic;
using TDC.Lib.Core.WPF.Controls.TurtlePath;

namespace TDC.Gadgets.Fractals.Tools
{
    public class LSystemParameters : TurtleParameters
    {
        // List of rules - used to transform words.
        public List<string> Rules { get; set; } = new List<string> {"F:F"};

        // List of symbols - used to transform final word in the turtle command path.
        public List<string> Symbols { get; set; } = new List<string>();

        // Initial word.
        public string Axiom { get; set; } = "F";

        // Now many iteration shall be done.
        public int Level { get; set; } = 4;
    }
}