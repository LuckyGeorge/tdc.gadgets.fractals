﻿using System.Collections.Generic;

namespace TDC.Gadgets.Fractals.Tools
{
    public static class LSystems
    {
        private static readonly Dictionary<string, LSystemParameters> _lsystems = new Dictionary<string, LSystemParameters>
        {
            {
                "Clouds", new LSystemParameters
                {
                    Axiom = "F++F++F",
                    Rules = new List<string>
                    {
                        "F:F+F--F+F",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "[:[",
                        "]:]",
                        "+:+",
                        "-:-"
                    },
                    Level = 7,
                    Distance = 10,
                    TurnAngle = 60,
                    StartAngle = 90.0,
                    StartThickness = 30,
                    EndThickness = 30,
                }
            },
            {
                "Fine Snowflake", new LSystemParameters
                {
                    Axiom = "F+F+F+F+F+F+F+F",
                    Rules = new List<string>
                    {
                        "F:F---F+F+F+F+F+F+F---F",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "[:[",
                        "]:]",
                        "+:+",
                        "-:-"
                    },
                    Level = 5,
                    Distance = 10,
                    TurnAngle = 45,
                    StartAngle = 90.0,
                    StartThickness = 30,
                    EndThickness = 30,
                }
            },
            {
                "Bush", new LSystemParameters
                {
                    Axiom = "F",
                    Rules = new List<string>
                    {
                        "F:FF[--F+F+F][+F-F-F]",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "[:[",
                        "]:]",
                        "+:+",
                        "-:-"
                    },
                    Level = 4,
                    Distance = 10,
                    TurnAngle = 30,
                    StartThickness = 30,
                    StartAngle = 90.0
                }
            },
            {
                "Dragon Curve", new LSystemParameters
                {
                    Axiom = "Fl",
                    Rules = new List<string>
                    {
                        "F:F",
                        "l:l+rF+",
                        "r:-Fl-r",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "l: ",
                        "r: ",
                        "+:+",
                        "-:-"
                    },
                    Level = 10,
                    Distance = 10,
                    TurnAngle = 90
                }
            },
            {
                "Islands and lakes", new LSystemParameters
                {
                    Level = 2,
                    Axiom = "F-F-F-F",
                    Rules = new List<string>
                    {
                        "F:F-f+FF-F-FF-Ff-FF+f-FF+F+FF+Ff+FFF",
                        "f:ffffff",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "f:f",
                        "+:+",
                        "-:-"
                    },
                    Distance = 10,
                    TurnAngle = 90
                }
            },
            {
                "Quadratic snowflake", new LSystemParameters
                {
                    Axiom = "-F-F-F-F",
                    Rules = new List<string>
                    {
                        "F:F-F+F+F-F",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "+:+",
                        "-:-"
                    },
                    Level = 4,
                    Distance = 10,
                    TurnAngle = 90,
                    StartAngle = 45
                }
            },
            {
                "Sierpinski curve", new LSystemParameters
                {
                    Level = 6,
                    Axiom = "A",
                    Rules = new List<string>
                    {
                        "A:B-A-B",
                        "B:A+B+A",
                    },
                    Symbols = new List<string>
                    {
                        "A:F",
                        "B:F",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 60,
                    Distance = 10
                }
            },
            {
                "Spiral", new LSystemParameters
                {
                    Axiom = "F",
                    Rules = new List<string>
                    {
                        "F:+Ff",
                        "f:+F",
                        "+: "
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "+:+"
                    },
                    Level = 9,
                    Distance = 10,
                    TurnAngle = 15,
                    StepHandler = p => { p.Distance *= 1.02; }
                }
            },
            {
                "Plant", new LSystemParameters
                {
                    StartAngle = 90,
                    Level = 3,
                    Axiom = "F",
                    Rules = new List<string>
                    {
                        "F:F[+F]F[-F]F",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 25.7,
                    Distance = 50,
                    StartThickness = 30
                }
            },
            {
                "Plant2", new LSystemParameters
                {
                    Level = 5,
                    Axiom = "X",
                    Rules = new List<string>
                    {
                        "F:FF",
                        "X:F-[[X]+X]+F[+FX]-X",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "X: ",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 22.5,
                    StartAngle = 90,
                    Distance = 50,
                    StartThickness = 100,
                    UseRandomColors = true
                }
            },
            {
                "Hexagonal Gosper Curve", new LSystemParameters
                {
                    Level = 4,
                    Axiom = "XF",
                    Rules = new List<string>
                    {
                        "F:F",
                        "X:X+YF++YF-FX--FXFX-YF+",
                        "Y:-FX+YFYF++YF+FX--FX-Y",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "X: ",
                        "Y: ",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 60,
                    Distance = 10
                }
            },
            {
                "River", new LSystemParameters
                {
                    StartAngle = 90,
                    Level = 5,
                    Axiom = "F",
                    Rules = new List<string>
                    {
                        "F:F[+F]-F+[-+++F]F",
                    },
                    Symbols = new List<string>
                    {
                        "F:F",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 25.7,
                    Distance = 20,
                    StartThickness = 30
                }
            },
            {
                "Leaf", new LSystemParameters
                {
                    StartAngle = 90,
                    Level = 6,
                    Axiom = "i",
                    Rules = new List<string>
                    {
                        "i:a[+i][-i]ai",
                        "a:aa"
                    },

                    Symbols = new List<string>
                    {
                        "i:F",
                        "a:F",
                        "+:+",
                        "-:-",
                        "[:[",
                        "]:]"
                    },
                    TurnAngle = 60,
                    Distance = 40,
                    StartThickness = 100
                }
            }
        };

        public static Dictionary<string, LSystemParameters> Predefined => _lsystems;
    }
}