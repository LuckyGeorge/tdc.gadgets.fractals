﻿using TDC.Gadgets.Fractals.ViewModels;
using TDC.Lib.Core.Caliburn.Metro;

namespace TDC.Gadgets.Fractals
{
    public class AppBootstrapper : CaliburnMetroCompositionBootstrapper<AppViewModel>
    {

    }
}