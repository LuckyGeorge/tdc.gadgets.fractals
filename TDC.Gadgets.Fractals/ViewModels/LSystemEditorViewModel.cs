﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Converters;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Caliburn.Micro;
using ControlzEx.Standard;
using TDC.Gadgets.Fractals.Interfaces;
using TDC.Lib.Core;
using TDC.Lib.Core.Collections;
using TDC.Lib.Core.WPF.Commands;

namespace TDC.Gadgets.Fractals.ViewModels
{
    public class LSystemEditorViewModel : Screen, IFractalViewModel, IDataErrorInfo
    {
        private IWindowManager _windowManager;

        private const int FRACTAL_WIDTH = 1024;
        private const int FRACTAL_HEIGHT = 1024;

        private string _axiom;
        private int _iterations = 3;
        private bool _isAxiomValid;
        private ICommand _deleteRuleCommand;
        private readonly RulesContainer _rulesContainer = new RulesContainer();
        private readonly ObservableRangeCollection<string> _generations = new ObservableRangeCollection<string>();

        #region ctor
        public LSystemEditorViewModel()
        {
            base.DisplayName = "Design Time Object";
        }

        [ImportingConstructor]
        public LSystemEditorViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
            base.DisplayName = "L-System Editor";
            
            _rulesContainer.Add(new Rule(_rulesContainer){ Key="F", Mapping= "F-F++F-F" });
            _axiom = "F++F++F";
            _iterations = 4;
            _isAxiomValid = true;

            _rulesContainer.CanGenerateEvent += RulesContainerOnCanGenerateEvent;

            //First Draw
            RulesContainerOnCanGenerateEvent(null, null);
        }

        #endregion

        #region events
        private void RulesContainerOnCanGenerateEvent(object sender, EventArgs eventArgs)
        {
            _generations.Clear();
            _generations.AddRange(Parser.Generate(Axiom, _rulesContainer, Math.Max(3,_iterations), true));
            var points = Parser.GeneradeAs2DLineList(Axiom, _rulesContainer, _iterations);

            if (points.Count > 0)
            {
                var minX = points.Min(p => Math.Min(p.X1, p.X2));
                var minY = points.Min(p => Math.Min(p.Y1, p.Y2));

                var maxX = points.Max(p => Math.Max(p.X1, p.X2));
                var maxY = points.Max(p => Math.Max(p.Y1, p.Y2));

                DrawLine(minX, minY, maxX, maxY, points);
            }
        }
        #endregion

        

        #region Properties
        public string Axiom
        {
            get => _axiom;
            set
            {
                if (_axiom != value && !string.IsNullOrEmpty(value))
                {
                    _axiom = value;
                    NotifyOfPropertyChange();
                    IsAxiomValid = Parser.ValidateAxiom(_rulesContainer, _axiom);
                    if (IsAxiomValid)
                        RulesContainerOnCanGenerateEvent(null, null);
                }
                else
                {
                    IsAxiomValid = false;
                }
            }
        }

        public int Iterations
        {
            get => _iterations;
            set
            {
                if (_iterations != value)
                {
                    _iterations = value;
                    NotifyOfPropertyChange();
                    RulesContainerOnCanGenerateEvent(null, null);
                }
            }
        }

        public int Angle
        {
            get => _rulesContainer?.Angle ?? 0;
            set
            {
                if (_rulesContainer != null && _rulesContainer.Angle != value)
                {
                    _rulesContainer.Angle = value;
                    NotifyOfPropertyChange();
                    RulesContainerOnCanGenerateEvent(null, null);
                }
            }
        }

        public int Distance
        {
            get => _rulesContainer?.Distance ?? 0;
            set
            {
                if (_rulesContainer != null && _rulesContainer.Distance != value)
                {
                    _rulesContainer.Distance = value;
                    NotifyOfPropertyChange();
                    RulesContainerOnCanGenerateEvent(null, null);
                }
            }
        }

        public bool IsAxiomValid
        {
            get => _isAxiomValid;
            set
            {
                if (value != _isAxiomValid)
                {
                    _isAxiomValid = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public RulesContainer Rules => _rulesContainer;

        public ObservableRangeCollection<String> Generations => _generations;

        public WriteableBitmap Fractal { get; set; } = BitmapFactory.New(FRACTAL_WIDTH, FRACTAL_HEIGHT);

        #endregion

        #region commands

        public ICommand DeleteRuleCommand => _deleteRuleCommand ?? (_deleteRuleCommand = new DelegateCommand(OnDeleteRule));

        private void OnDeleteRule(object obj)
        {
            var rule = obj as Rule;
            if (rule != null)
            {
                var msgBoxResult = MessageBox.Show("Regel wirklich löschen?", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(msgBoxResult == MessageBoxResult.Yes)
                    _rulesContainer.Remove(rule);
            }
        }

        #endregion

        #region Methods

        private void DrawLine(double minX, double minY, double maxX, double maxY, List<Line2D> lines, int margin = 10)
        {
            var sw = new Stopwatch();
            sw.Start();

            var xRange = maxX - minX;
            var yRange = maxY - minY;

            //Center image
            if (xRange > yRange)
                minY -= (xRange - yRange) / 2;
            else
                minX -= (yRange - xRange) / 2;

            var scaleX = xRange / (FRACTAL_WIDTH - 2 * margin);
            var scaleY = yRange / (FRACTAL_HEIGHT - 2 * margin);
            var scale = Math.Max(scaleX, scaleY);

            for (var i = 0; i < lines.Count; i++)
            {
                lines[i] = new Line2D((lines[i].X1 - minX) / scale + margin,
                                      (lines[i].Y1 - minY) / scale + margin,
                                      (lines[i].X2 - minX) / scale + margin,
                                      (lines[i].Y2 - minY) / scale + margin);
            }

            Execute.OnUIThread(() =>
            {
                Fractal.Clear(Colors.AntiqueWhite);

                using (Fractal.GetBitmapContext())
                {
                    foreach (var line in lines)
                    {
                        
                        Fractal.DrawLineBresenham((int)line.X1, (int)line.Y1, (int)line.X2, (int)line.Y2, Colors.Black);
                    }
                }

                NotifyOfPropertyChange(nameof(Fractal));
            });

            Console.WriteLine($"Drawing took {sw.ElapsedMilliseconds} ms");
        }


        #endregion

        #region DTO

        public class Rule : PropertyChangedBase, IDataErrorInfo
        {
            private string _key;
            private string _mapping;
            private bool _isKeyValid;
            private bool _isMappingValid;
            private bool _isParentesisValid;
            private RulesContainer _container;

            public Rule()
            {
            }

            public Rule(RulesContainer container)
            {
                _container = container;
            }

            public void SetContainer(RulesContainer container)
            {
                _container = container;
                Parser.ValidateRule(_container, this);
            }

            public bool IsValid => IsKeyValid && IsMappingValid && IsParentesisValid;

            public string Key
            {
                get => _key;
                set
                {
                    if (_key != value && value != null && value.Length == 1)
                    {
                        _key = value;
                        NotifyOfPropertyChange();
                        Parser.ValidateRule(_container, this);
                        _container?.RaiseGenerateEventIfAllItemsAreValid();
                    }
                    else
                    {
                        IsKeyValid = false;
                    }
                }
            }

            public bool IsKeyValid
            {
                get => _isKeyValid;
                set
                {
                    if (value != _isKeyValid)
                    {
                        _isKeyValid = value;
                        NotifyOfPropertyChange();
                        NotifyOfPropertyChange(nameof(IsValid));
                    }
                }
            }

            public string Mapping
            {
                get => _mapping;
                set
                {
                    if (_mapping != value && value != null && value.Length >= 1)
                    {
                        _mapping = value;
                        NotifyOfPropertyChange();
                        Parser.ValidateRule(_container, this);
                        _container?.RaiseGenerateEventIfAllItemsAreValid();
                    }
                    else
                    {
                        IsMappingValid = false;
                    }
                }
            }

            public bool IsMappingValid
            {
                get => _isMappingValid;
                set
                {
                    if (value != _isMappingValid)
                    {
                        _isMappingValid = value;
                        NotifyOfPropertyChange();
                        NotifyOfPropertyChange(nameof(IsValid));
                    }
                }
            }

            public bool IsParentesisValid
            {
                get => _isParentesisValid;
                set
                {
                    if (value != _isParentesisValid)
                    {
                        _isParentesisValid = value;
                        NotifyOfPropertyChange();
                        NotifyOfPropertyChange(nameof(IsValid));
                    }
                }
            }

            public RulesContainer Container => _container;

            public string this[string columnName]
            {
                get
                {
                    string result = null;
                    if (columnName == nameof(Key))
                    {
                        if (!IsKeyValid)
                            result = "Ungültige Variable!";
                    }
                    if (columnName == nameof(Mapping))
                    {
                        if (!IsMappingValid)
                            result = "Ungültige Abbildung!";
                        if (!IsParentesisValid)
                            result += "Klammerfehler!";
                    }
                    return result;
                }
            }

            public string Error { get; }
        }

        public class RulesContainer : ObservableCollection<Rule>
        {
            public event EventHandler CanGenerateEvent;

            public int Distance { get; set; } = 20;

            public int Angle { get; set; } = 60;

            public double RandomizeLength { get; set; } = 0.3;

            public double RandomizeAngle { get; set; } = 0.4;

            public string GetFreeRule()
            {
                for (byte i = 0x41; i < 0x5A; i++)
                {
                    var variable = Convert.ToChar(i).ToString();
                    if (this.All(v => v.Key != variable))
                        return variable;
                }
                return String.Empty;
            }

            public void RaiseGenerateEventIfAllItemsAreValid()
            {
                ValidateAll();
                if (this.All(r => r.IsValid))
                    CanGenerateEvent?.Invoke(this, null);
            }

            protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                base.OnCollectionChanged(e);
                if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems != null && e.NewItems.Count > 0)
                {
                    ValidateAll();
                }
            }

            private void ValidateAll()
            {
                foreach (var rule in this)
                {
                    if (rule.Container == null)
                        rule.SetContainer(this);
                    else
                        Parser.ValidateRule(this, rule);
                }
            }
        }

        #endregion

        public string this[string columnName]
        {
            get {
                string result = null;
                if (columnName == nameof(Axiom))
                {
                    if (!IsAxiomValid)
                        result = "Ungültiges Startwort!";
                }
                return result;
            }
        }

        public string Error { get; }
    }

    public static class Parser
    {
        #region nested classes
        public class State
        {
            public double Length { get; set; }

            public double Angle { get; set; }

            public double X { get; set; }

            public double Y { get; set; }

            public double Direction { get; set; }

            public State Clone() { return (State)this.MemberwiseClone(); }
        }
        #endregion

        private static readonly List<string> ValidCommands = new List<string>{
            "F", //-> Forward, drawing a line
            "f", //-> Forward without drawing a line
            "+", //-> turn left by default angle
            "-", //-> turn right by default angle
            //"\\",//-> roll left by default angle
            //"/", //-> roll right by default angle
            //"^", //-> pitch up by default angle
            //"&", //-> pitch down by default angle
            "|", //-> turn around by 180 degrees
            //"J", //-> insert point (joint) at current position
            "@",//-> multiply current length by default length scale
            "!", //-> mirror angle (switch the meaning of left or right)
            "[", //-> start branch (push turtle state)
            "]", //-> end branch (pop turtle state)
            "(", //-> start of a custom value
            ")", //-> end of a custom value
        };

        public static void ValidateRule(LSystemEditorViewModel.RulesContainer container, LSystemEditorViewModel.Rule rule)
        {
            rule.IsKeyValid = false;
            rule.IsMappingValid = false;
            rule.IsParentesisValid = false;

            if (container != null && rule != null)
            {
                rule.IsKeyValid = !String.IsNullOrEmpty(rule.Key) && rule.Key.Length == 1 && ((rule.Key[0] >= 0x41 && rule.Key[0] <= 0x5A) || (rule.Key[0] >= 0x61 && rule.Key[0] <= 0x7A)) && container.Where(st => st != rule).All(st => st.Key != rule.Key);

                if (!String.IsNullOrEmpty(rule.Mapping))
                {
                    var checkFlags = new bool[rule.Mapping.Length];

                    int branchCount = 0;
                    int parentesisCount = 0;

                    // Check Parentesis and branches
                    for (int i = 0; i < rule.Mapping.Length; i++)
                    {
                        if (rule.Mapping[i] == '[' && parentesisCount == 0) branchCount++;
                        if (rule.Mapping[i] == ']' && i > 0 && rule.Mapping[i - 1] != '[' && parentesisCount == 0) branchCount--;
                        if (rule.Mapping[i] == '(' && branchCount == 0) parentesisCount++;
                        if (rule.Mapping[i] == ')' && i > 0 && rule.Mapping[i - 1] != '(' && branchCount == 0) parentesisCount--;
                    }

                    if (parentesisCount == 0 && branchCount == 0)
                    {
                        rule.IsParentesisValid = true;

                        //Check for commands and variables
                        for (int i = 0; i < rule.Mapping.Length; i++)
                        {
                            if (ValidCommands.Any(vc => vc == rule.Mapping[i].ToString()) || container.Any(r=>r.Key == rule.Mapping[i].ToString()))
                            {
                                checkFlags[i] = true;
                            }
                        }

                        //Check for custom values
                        for (int i = 0; i < rule.Mapping.Length; i++)
                        {
                            if (rule.Mapping[i] == '(')
                            {
                                var customValue = GetCustomValue(rule.Mapping, i, out int endOfCustomValue);
                                if (customValue != null)
                                {
                                    for(int j=i;j<=endOfCustomValue;j++)
                                        checkFlags[j] = true;
                                    i = endOfCustomValue;

                                }
                                else checkFlags[i] = false;
                            }
                        }

                        rule.IsMappingValid = rule.IsParentesisValid && checkFlags.All(f => f);
                    }
                }
            }
        }

        public static bool ValidateAxiom(LSystemEditorViewModel.RulesContainer container, string axiom)
        {
            if (container != null && !String.IsNullOrEmpty(axiom))
            {
                return axiom.All(value => ValidCommands.Any(c => c == value.ToString()) || container.Any(r => r.Key == value.ToString()));
            }

            return false;
        }

        public static List<string> Generate(string axiom, LSystemEditorViewModel.RulesContainer container, int maxIterations, bool isPreview)
        {
            var generations = new List<string>();
            if (ValidateAxiom(container,axiom) && container != null && container.All(r => r.IsValid))
            {
                for (int i = 0; i < maxIterations; i++)
                {
                    String lastGeneration = i == 0 ? axiom : generations[i - 1];

                    var currentGeneration = GenerateGeneration(container, lastGeneration);

                    if (currentGeneration == lastGeneration && i > 0 || (isPreview && currentGeneration.Length >= 250)) return generations;
                    generations.Add(currentGeneration);
                }
            }
            return generations;
        }

        private static string GenerateGeneration(LSystemEditorViewModel.RulesContainer container, string lastGeneration)
        {
            var stringBuilder = new StringBuilder();
            foreach (var ch in lastGeneration)
            {
                var rule = container.FirstOrDefault(r => r.Key == ch.ToString());
                if (rule != null)
                {
                    stringBuilder.Append(rule.Mapping);
                }
                else stringBuilder.Append(ch);
            }
            return stringBuilder.ToString();
        }

        public static List<Line2D> GeneradeAs2DLineList(string axiom, LSystemEditorViewModel.RulesContainer container, int maxIterations)
        {
            var result = new List<Line2D>();
            var lastGeneration = String.Empty;
            
            String currentGeneration = String.Empty;

            if (ValidateAxiom(container, axiom) && container != null && container.All(r => r.IsValid))
            {
                var sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < maxIterations; i++)
                {
                    if (String.IsNullOrEmpty(lastGeneration)) lastGeneration = axiom;

                    currentGeneration = GenerateGeneration(container, lastGeneration);

                    if (currentGeneration == lastGeneration && i > 0) break;
                    lastGeneration = currentGeneration;
                }

                Console.WriteLine($"L-System Generation took {sw.ElapsedMilliseconds} ms");
                sw.Restart();
                //Replace all Variables that are no commands to constants
                var stringBuilder = new StringBuilder();
                foreach (var value in currentGeneration)
                {
                    if (ValidCommands.All(vc => vc != value.ToString()) && !Char.IsDigit(value))
                    {
                        if (Char.IsUpper(value))
                            stringBuilder.Append('F');
                        else if (Char.IsLower(value))
                            stringBuilder.Append('f');
                    }
                    stringBuilder.Append(value);
                }
                var drawString = stringBuilder.ToString();
                Console.WriteLine($"Drawstring Generation took {sw.ElapsedMilliseconds} ms");

                var states = new Stack<State>();
                var currentState = new State
                {
                    X = 512,
                    Y = 512,
                    Direction = 0,
                    Length = container.Distance,
                    Angle = container.Angle
                };

                
                for (var i = 0; i < drawString.Length; i++)
                {
                    if (drawString[i] == 'F' || drawString[i] == 'f')
                    {
                        var size = currentState.Length;
                        var newI = i;
                        if (i < drawString.Length - 1 && drawString[i + 1] == '(')
                        {
                            var customValue = GetCustomValue(drawString, i + 1, out int endOfCustomValue);
                            if (customValue.HasValue)
                            {
                                newI = endOfCustomValue;
                                size = customValue.Value;
                            }
                        }

                        if (container.RandomizeLength > 0)
                        {
                            var random = new Random(i);
                            size = size - (size + 2 * (random.NextDouble() * size * container.RandomizeLength));
                        }
                        var newX = currentState.X + size *
                                   Math.Cos(currentState.Direction * Math.PI / 180.0);
                        var newY = currentState.Y + size *
                                   Math.Sin(currentState.Direction * Math.PI / 180.0);

                        if (drawString[i] == 'F')
                            result.Add(new Line2D(currentState.X, currentState.Y, newX, newY));

                        currentState.X = newX;
                        currentState.Y = newY;

                        i = newI;
                    }
                    else if (drawString[i] == '+')
                    {
                        var customValue = GetCustomValue(drawString, i+1, out int endOfCustomValue);
                        currentState.Direction += customValue ?? currentState.Angle;
                        if (customValue != null)
                            i = endOfCustomValue;
                    }

                    else if (drawString[i] == '-')
                    {
                        var customValue = GetCustomValue(drawString, i+1, out int endOfCustomValue);
                        currentState.Direction -= customValue ?? currentState.Angle;
                        if(customValue != null)
                            i = endOfCustomValue;
                    }

                    else if (drawString[i] == '[') states.Push(currentState.Clone());

                    else if (drawString[i] == ']') currentState = states.Pop();

                    else if (drawString[i] == '!') currentState.Angle *= -1.0;

                    else if (drawString[i] == '|') currentState.Direction += 180.0;
                }
            }
            return result;
        }

        private static double? GetCustomValue(string drawString, int index, out int endOfCustomValue)
        {
            endOfCustomValue = index;
            if (drawString[index] == '(')
            {
                var stringBuilder = new StringBuilder();
                var endFound = false;
                for (int j = index + 1; j < index + 32; j++)
                {
                    if (drawString[j] != ')' && (char.IsDigit(drawString[j]) || char.IsPunctuation(drawString[j])))
                        stringBuilder.Append(drawString[j]);
                    else if(drawString[j] == ')')
                    {
                        endOfCustomValue = j;
                        endFound = true;
                        break;
                    }
                    else
                    {
                        endOfCustomValue = j;
                        endFound = false;
                        break;
                    }
                }

                if (!endFound) return null;

                var value = stringBuilder.ToString().Replace(',', '.');
                if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out double result))
                    return result;
            }
            return null;
        }
    }


}