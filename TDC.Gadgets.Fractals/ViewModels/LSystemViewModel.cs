﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using TDC.Gadgets.Fractals.Interfaces;
using TDC.Gadgets.Fractals.Tools;
using TDC.Lib.Core.WPF.Controls.TurtlePath;
using Action = System.Action;

namespace TDC.Gadgets.Fractals.ViewModels
{
    public class LSystemViewModel : Screen, IFractalViewModel
    {
        private bool _isBusy;

        

        private string _path;
        private TurtleParameters _turtleParameters;
        private IWindowManager _windowManager;
        private string _selectedLSystem;

        private void Build(LSystemParameters parameters)
        {
            var rulesDict = new Dictionary<char, string>();
            if (parameters.Rules != null)
                foreach (var s in parameters.Rules)
                {
                    if (s.Length < 3 || s[1] != ':')
                        throw new InvalidOperationException("Wrong rule text");

                    rulesDict.Add(s[0], s.Substring(2));
                }

            var symbolsDict = new Dictionary<char, char>();
            if (parameters.Symbols != null)
                foreach (var s in parameters.Symbols)
                {
                    if (s.Length != 3 || s[1] != ':')
                        throw new InvalidOperationException("Wrong symbols text");

                    symbolsDict.Add(s[0], s[2]);
                }

            var path = parameters.Axiom;
            for (var i = 0; i < parameters.Level; ++i)
            {
                var newPath = new StringBuilder();
                foreach (var c in path)
                    if (rulesDict.ContainsKey(c))
                        newPath.Append(rulesDict[c]);
                    else
                        newPath.Append(c);

                path = newPath.ToString();
            }

            var translatedPath = new StringBuilder();
            foreach (var c in path)
                if (symbolsDict.ContainsKey(c))
                    translatedPath.Append(symbolsDict[c]);

            Path = translatedPath.ToString();
            TurtleParameters = parameters;
        }

        private void CalculateLSystem()
        {
            Build(Tools.LSystems.Predefined.Values.First());
        }

        #region Commands

        public async void StartCalculationCommand()
        {
            Execute.OnUIThread(() => IsBusy = true);
            await Task.Run(new Action(CalculateLSystem));
            Execute.OnUIThread(() => IsBusy = false);
        }

        #endregion

        #region ctor

        public LSystemViewModel()
        {
            DisplayName = "Design Time Object";
        }

        [ImportingConstructor]
        public LSystemViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
            DisplayName = "L-System";
            SelectedLSystem = Tools.LSystems.Predefined.Keys.First();
        }

        #endregion

        #region Properties

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy != value)
                {
                    _isBusy = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public List<String> LSystems => new List<string>(Tools.LSystems.Predefined.Keys);

        public string SelectedLSystem
        {
            get => _selectedLSystem;
            set
            {
                if (_selectedLSystem != value)
                {
                    _selectedLSystem = value;
                    NotifyOfPropertyChange();

                    var parameter = Tools.LSystems.Predefined[_selectedLSystem];
                    Build(parameter);
                }
            }
        }

        public TurtleParameters TurtleParameters
        {
            get => _turtleParameters;
            set
            {
                if (_turtleParameters != value)
                {
                    _turtleParameters = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public string Path
        {
            get => _path;

            set
            {
                if (_path != value)
                {
                    _path = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        #endregion
    }
}