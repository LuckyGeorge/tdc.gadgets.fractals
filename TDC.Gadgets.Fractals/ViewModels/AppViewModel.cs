﻿using System.ComponentModel.Composition;
using Caliburn.Micro;

namespace TDC.Gadgets.Fractals.ViewModels
{
    [Export(typeof(AppViewModel))]
    public class AppViewModel : Conductor<object>
    {
        private readonly IWindowManager _windowManager;

        [ImportingConstructor]
        public AppViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
            this.Activated += (sender, args) => ActivateItem(new FractalDesignerViewModel(_windowManager));
        }
    }
}