﻿using System.ComponentModel.Composition;
using System.Linq;
using Caliburn.Micro;
using TDC.Gadgets.Fractals.Interfaces;
using TDC.Lib.Core.Collections;

namespace TDC.Gadgets.Fractals.ViewModels
{
    [Export(typeof(FractalDesignerViewModel))]
    public class FractalDesignerViewModel : Screen
    {
        private readonly IWindowManager _windowManager;
        private IFractalViewModel _currentFractalViewModel;

        #region ctor
        public FractalDesignerViewModel()
        {
            base.DisplayName = "Design Time Object";

            FractalViewModels.Add(new DesignTimeFractalViewModel());
            FractalViewModels.Add(new DesignTimeFractalViewModel());
            FractalViewModels.Add(new DesignTimeFractalViewModel());
            FractalViewModels.Add(new DesignTimeFractalViewModel());
        }

        [ImportingConstructor]
        public FractalDesignerViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
            base.DisplayName = "Fractal Designer";

            FractalViewModels.Add(new MandelbrotViewModel(_windowManager));
            FractalViewModels.Add(new LSystemViewModel(_windowManager));
            FractalViewModels.Add(new LSystemEditorViewModel(_windowManager));
            CurrentFractalViewModel = FractalViewModels.Last();
        }
        #endregion

        #region Properties

        public ObservableRangeCollection<IFractalViewModel> FractalViewModels { get; } = new ObservableRangeCollection<IFractalViewModel>();

        public IFractalViewModel CurrentFractalViewModel
        {
            get => _currentFractalViewModel;
            set
            {
                if (_currentFractalViewModel == value) return;
                _currentFractalViewModel = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion
    }
}