﻿using Caliburn.Micro;
using TDC.Gadgets.Fractals.Interfaces;

namespace TDC.Gadgets.Fractals.ViewModels
{
    public class DesignTimeFractalViewModel : Screen, IFractalViewModel
    {
        public DesignTimeFractalViewModel()
        {
            base.DisplayName = "Design Time Fractal";
        }
    }
}