﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using TDC.Gadgets.Fractals.Interfaces;
using TDC.Gadgets.Fractals.Tools;
using Action = System.Action;

namespace TDC.Gadgets.Fractals.ViewModels
{
    [Export(typeof(MandelbrotViewModel))]
    public class MandelbrotViewModel : Screen, IFractalViewModel
    {
        private const int FRACTAL_WIDTH = 1024;
        private const int FRACTAL_HEIGHT = 1024;
        private const int MAXIMUM_ITERATIONS = 10000;

        private const int DEFAULT_ITERATIONS = 250;
        private const double DEFAULT_START_X = -2D;
        private const double DEFAULT_END_X = 1D;
        private const double DEFAULT_OFFSET_Y = 0D;
        private const int NUMBER_OF_LINES_PER_RENDER_CALL = 64;
        private const double MAX_MAGNITUDE = 2D;
        private const double MAX_MAGNITUDE_SQUARED = MAX_MAGNITUDE * MAX_MAGNITUDE;

        private readonly IWindowManager _windowManager;

        /// <summary>
        /// The start value of the real part
        /// </summary>
        private double _startX = DEFAULT_START_X;

        /// <summary>
        /// The end value of the real part
        /// </summary>
        private double _endX = DEFAULT_END_X;

        /// <summary>
        /// The offset of the imaginary part
        /// </summary>
        private double _yOffset = DEFAULT_OFFSET_Y;

        private int _iterations = DEFAULT_ITERATIONS;
        private bool _isBusy;
        private double _mouseY;
        private double _mouseX;
        private double _realValueUnderMouse;
        private double _imaginaryValueUnderMouse;
        private bool _isSelecting;
        private double _selectionStartY;
        private double _selectionStartX;
        private double _selectionEndX;
        private double _selectionEndY;
        private double _selectionStartReal;
        private double _selectionStartImaginary;
        private double _selectionEndReal;
        private double _selectionEndImaginary;
        private PaletteType _palette = PaletteType.WarmAutumn;
        private int _bytesPerPixel = 4;

        #region ctor
        public MandelbrotViewModel()
        {
            base.DisplayName = "Design Time Object";
        }

        [ImportingConstructor]
        public MandelbrotViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
            base.DisplayName = "Mandelbrot";
        }
        #endregion


        #region commands
        public async void StartCalculationCommand()
        {
            Execute.OnUIThread(() => IsBusy = true);
            await Task.Run(new Action(CalculateFractal));
            Execute.OnUIThread(() => IsBusy = false);
        }

        public async void ResetToDefaultCommand()
        {
            Execute.OnUIThread(() => Iterations = DEFAULT_ITERATIONS);
            Execute.OnUIThread(() => StartReal = DEFAULT_START_X);
            Execute.OnUIThread(() => EndReal = DEFAULT_END_X);
            Execute.OnUIThread(() => ImaginaryOffset = DEFAULT_OFFSET_Y);

            Execute.OnUIThread(() => IsBusy = true);
            await Task.Run(new Action(CalculateFractal));
            Execute.OnUIThread(() => IsBusy = false);
        }

        #endregion

        #region drawing methods

        private void DrawLine(byte[] line, int lineIndex ,int lineWidth, int stride)
        {
            if (lineWidth < 0 || lineIndex < 0 || lineWidth > FRACTAL_WIDTH || lineIndex >= FRACTAL_HEIGHT) return;

            var srcRect = new Int32Rect(0, 0, lineWidth, 1);

            Execute.OnUIThread(() =>
            {
                using (Fractal.GetBitmapContext())
                {
                    Fractal.WritePixels(srcRect, line, lineWidth* stride, 0, lineIndex);
                }
            });
        }

        private void DrawLines(byte[] lines, int startLineIndex, int numberOfLines, int lineWidth, int stride)
        {
            if (lineWidth < 0 || startLineIndex < 0 || lineWidth > FRACTAL_WIDTH || startLineIndex + numberOfLines > FRACTAL_HEIGHT) return;

            var srcRect = new Int32Rect(0, 0, lineWidth, numberOfLines);

            Execute.OnUIThread(() =>
            {
                using (Fractal.GetBitmapContext())
                {
                    Fractal.WritePixels(srcRect, lines, lineWidth * stride, 0, startLineIndex);
                }
            });
        }

        private void ClearFractal()
        {
            Execute.OnUIThread(() =>
            {
                using (Fractal.GetBitmapContext())
                {
                    Fractal.Clear(Colors.AntiqueWhite);
                }
            });
        }
        #endregion

        #region properties
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy != value)
                {
                    _isBusy = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public WriteableBitmap Fractal { get; } = BitmapFactory.New(FRACTAL_WIDTH, FRACTAL_HEIGHT);

        public double Width { get; } = FRACTAL_WIDTH;

        public double Height { get; } = FRACTAL_HEIGHT;

        public int Iterations
        {
            get => _iterations;
            set
            {
                if (_iterations != value && value > 0 && value <= MAXIMUM_ITERATIONS)
                {
                    _iterations = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public double StartReal
        {
            get => _startX;
            set
            {
                if (Math.Abs(_startX - value) > Double.Epsilon)
                {
                    _startX = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public double EndReal
        {
            get => _endX;
            set
            {
                if (Math.Abs(_endX - value) > Double.Epsilon)
                {
                    _endX = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public double ImaginaryOffset
        {
            get => _yOffset;
            set
            {
                if (Math.Abs(_yOffset - value) > Double.Epsilon)
                {
                    _yOffset = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public List<PaletteType> Palettes => new List<PaletteType>(Enum.GetValues(typeof(PaletteType)).Cast<PaletteType>());

        public PaletteType SelectedPalette
        {
            get => _palette;
            set
            {
                if (value != _palette)
                {
                    _palette = value;
                    NotifyOfPropertyChange();
                    StartCalculationCommand();
                }
            }
        }

        public double RealValueUnderMouse => _realValueUnderMouse;

        public double ImaginaryValueUnderMouse => _imaginaryValueUnderMouse;

        public double MouseX
        {
            get => _mouseX;
            set
            {
                if (Math.Abs(_mouseX - value) > Double.Epsilon)
                {
                    _mouseX = value;
                    _realValueUnderMouse = MouseXToRealValue(_mouseX);
                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(()=> RealValueUnderMouse);
                }
            }
        }

        public double MouseY
        {
            get => _mouseY;
            set
            {
                if (Math.Abs(_mouseY - value) > Double.Epsilon)
                {
                    _mouseY = value;
                    _imaginaryValueUnderMouse = MouseYToImaginaryValue(_mouseY);

                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(() => ImaginaryValueUnderMouse);
                }
            }
        }

        public bool IsSelecting
        {
            get => _isSelecting;
            set
            {
                if (_isBusy) return;

                if (!_isSelecting && value)
                {
                    _isSelecting = true;
                    SelectionEndX = SelectionStartX;
                    SelectionEndY = SelectionStartY;
                    NotifyOfPropertyChange();
                }
                if (_isSelecting && !value)
                {
                    _isSelecting = false;
                    NotifyOfPropertyChange();

                    StartReal = SelectionStartReal;
                    EndReal = SelectionEndReal;

                    ImaginaryOffset = -(SelectionStartImaginary + SelectionEndImaginary) / 2;
                    var calculationAction = new Action(StartCalculationCommand);
                    calculationAction.BeginInvoke(null, null);
                }
            }
        }

        public double SelectionStartX
        {
            get => _selectionStartX;
            set
            {
                if (!_isSelecting) return;

                if (Math.Abs(_selectionStartX - value) > Double.Epsilon)
                {
                    _selectionStartX = value;
                    _selectionStartReal = MouseXToRealValue(_selectionStartX);
                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(() => SelectionStartReal);
                }
            }
        }

        public double SelectionStartReal => _selectionStartReal;

        public double SelectionStartY
        {
            get => _selectionStartY;
            set
            {
                if (!_isSelecting) return;

                if (Math.Abs(_selectionStartY - value) > Double.Epsilon)
                {
                    _selectionStartY = value;
                    _selectionStartImaginary = MouseYToImaginaryValue(_selectionStartY);
                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(() => SelectionStartImaginary);
                }
            }
        }

        public double SelectionStartImaginary => _selectionStartImaginary;

        public double SelectionEndX
        {
            get => _selectionEndX;
            set
            {
                if (!_isSelecting) return;

                if (Math.Abs(_selectionEndX - value) > Double.Epsilon)
                {
                    _selectionEndX = value;
                    _selectionEndReal = MouseXToRealValue(_selectionEndX);
                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(() => SelectionEndReal);
                    NotifyOfPropertyChange(() => SelectionWidth);
                }
            }
        }

        public double SelectionEndReal => _selectionEndReal;

        public double SelectionEndY
        {
            get => _selectionEndY;
            set
            {
                if (!_isSelecting) return;

                if (Math.Abs(_selectionEndY - value) > Double.Epsilon)
                {
                    _selectionEndY = value;
                    _selectionEndImaginary = MouseYToImaginaryValue(_selectionEndY);
                    NotifyOfPropertyChange();
                    NotifyOfPropertyChange(() => SelectionEndImaginary);
                    NotifyOfPropertyChange(() => SelectionHeight);
                }
            }
        }

        public double SelectionWidth => SelectionEndX - SelectionStartX;

        public double SelectionHeight => SelectionEndY - SelectionStartY;

        public double SelectionEndImaginary => _selectionEndImaginary;

        #endregion

        #region Mousehelper

        private double MouseXToRealValue(double mouseX)
        {
            var deltaPerImagePixel = (_endX - _startX) / (FRACTAL_WIDTH - 1D);
            return _startX + mouseX * deltaPerImagePixel;
        }

        private double MouseYToImaginaryValue(double mouseY)
        {
            var deltaPerImagePixel = (_endX - _startX) / (FRACTAL_WIDTH - 1D);
            var imaginaryStart = Math.Abs((FRACTAL_HEIGHT / 2.0) * deltaPerImagePixel) - _yOffset;
            return imaginaryStart - mouseY * deltaPerImagePixel;
        }

        #endregion

        #region Calculations - From https://github.com/sunsided/mandelbrot/blob/master/mandelbrot/BrotForm.cs

        private void CalculateFractal()
        {
            ClearFractal();
            RenderMandelbrotSet(FRACTAL_HEIGHT, FRACTAL_WIDTH, _bytesPerPixel);
        }

        /// <summary>
        /// Renders the Mandelbrot Set to the WriteableBitmap
        /// </summary>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="bytesPerPixel"></param>
        private void RenderMandelbrotSet(int height, int width, int bytesPerPixel)
        {
            var realDeltaPerImagePixel =(_endX - _startX) / (FRACTAL_WIDTH - 1D);
            var imagDeltaPerImagePixel = -realDeltaPerImagePixel;
            var imaginaryStart = Math.Abs((FRACTAL_HEIGHT / 2.0) * realDeltaPerImagePixel) - _yOffset;

            // iterate over all lines and columns
            var options = new ParallelOptions
            {
                MaxDegreeOfParallelism = Environment.ProcessorCount / 2
            };

            Parallel.For(0, height / NUMBER_OF_LINES_PER_RENDER_CALL, options, count =>
            {
                var y = count * NUMBER_OF_LINES_PER_RENDER_CALL;
                var realStartPosition = new Complex(_startX, imaginaryStart + (y * imagDeltaPerImagePixel));
                var lines = RenderLines(FRACTAL_WIDTH, NUMBER_OF_LINES_PER_RENDER_CALL, bytesPerPixel, realStartPosition, realDeltaPerImagePixel, imagDeltaPerImagePixel, _iterations, _palette);
                DrawLines(lines, y, NUMBER_OF_LINES_PER_RENDER_CALL, width, bytesPerPixel);
            });

        }

        /// <summary>
        /// Renders multiple lines of the fractal
        /// </summary>
        /// <param name="lineWidth">The width the image</param>
        /// <param name="numberOfLines">Number of lines to render</param>
        /// <param name="bytesPerPixel">The size of a Pixel in byte</param>
        /// <param name="fractalPixel">The fractal start position</param>
        /// <param name="realIncrease">The real increment per pixel of a fractal position</param>
        /// <param name="imagIncrease">The imaginary increment per pixel of a fractal position</param>
        /// <param name="maxIterations">The maximum iterations.</param>
        /// <param name="palette">The color set to use <see cref="Palette"/></param>
        private static byte[] RenderLines(int lineWidth, int numberOfLines, int bytesPerPixel, Complex fractalPixel, double realIncrease, double imagIncrease, int maxIterations, PaletteType palette)
        {
            var lineBuffer = new byte[numberOfLines * lineWidth * bytesPerPixel];

            var startReal = fractalPixel.Real;
            var startImag = fractalPixel.Imaginary;
            for (var y = 0; y < numberOfLines; y++)
            {
                fractalPixel = new Complex(startReal, startImag + imagIncrease * y);
                for (var x = 0; x < lineWidth * bytesPerPixel; x += bytesPerPixel)
                {
                    var pos = (y * lineWidth * bytesPerPixel) + x;
                    SetColorAtLocation(ref lineBuffer, pos, ref fractalPixel, palette, maxIterations);
                    fractalPixel += new Complex(realIncrease, 0);
                }
            }

            return lineBuffer;
        }

        /// <summary>
        /// Sets the color of the <paramref name="pixel"/> at the given <paramref name="location"/>
        /// </summary>
        /// <param name="buffer">The byte array of all Pixels</param>
        /// <param name="startIndex">The start index of the pixel</param>
        /// <param name="location">The pixel position.</param>
        /// <param name="palette">The color set to use <see cref="Palette"/></param>
        /// <param name="maxIterations">The maximum iterations.</param>
        private static void SetColorAtLocation(ref byte[] buffer, int startIndex, ref Complex location, PaletteType palette, int maxIterations)
        {
            int blue = startIndex, green = startIndex + 1, red = startIndex + 2, alpha = startIndex + 3;

            // do the Mandelmagic
            var iterations = DetermineNumberOfIterationsRequired(ref location, maxIterations);
            var color = Palette.GetColor(iterations, maxIterations, palette);
            buffer[blue] = color[2];
            buffer[green] = color[1];
            buffer[red] = color[0];
            buffer[alpha] = 255;
        }

        /// <summary>
        /// Determines how many iterations are required to determine unboundedness.
        /// </summary>
        /// <param name="location">The pixel position.</param>
        /// <param name="maxIterations">The maximum iterations.</param>
        /// <returns>System.Double.</returns>
        private static double DetermineNumberOfIterationsRequired(ref Complex location, int maxIterations)
        {
            Complex finalZ;
            var iterations = CalculateFractalIterations(location, maxIterations, out finalZ);
            //var smoothIterations = InterpolateIterations(maxIterations, iterations, ref finalZ);
            //return smoothIterations;
            return iterations;
        }

        /// <summary>
        /// Interpolates the iterations so that it becomes smoother.
        /// </summary>
        /// <param name="maxIterations">The maximum iterations.</param>
        /// <param name="iterations">The iterations.</param>
        /// <param name="finalZ">The final z.</param>
        /// <returns>System.Double.</returns>
        private static double InterpolateIterations(int maxIterations, int iterations, ref Complex finalZ)
        {
            // (https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Mandelbrot_set#Real_Escape_Time)
            return iterations < maxIterations ? iterations + 1.0 - Math.Log(Math.Log(finalZ.Magnitude, 2D), 2D) : 0D;
        }

        /// <summary>
        /// Determines the number of iterations required for the Mandelbrot
        /// fractal <c>Z_{n+1}(c) = z^2_{n}(c) + c</c> to reach the value of <c>2</c>.
        /// </summary>
        /// <param name="c">The location in the complex plane.</param>
        /// <param name="maxIterations">The maximum number of iterations.</param>
        /// <param name="z">The z.</param>
        /// <returns>System.Int32.</returns>
        private static int CalculateFractalIterations(Complex c, int maxIterations, out Complex z)
        {
            var iteration = 0;

            // iterate until the maximum magnitude is reached
            z = Complex.Zero;
            
            while (++iteration < maxIterations)
            {
                z = z * z + c;
                var squaredNorm = z.Real * z.Real + z.Imaginary * z.Imaginary;
                if (squaredNorm >= MAX_MAGNITUDE_SQUARED) break;
            }

            return iteration;
        }
        #endregion
    }
}